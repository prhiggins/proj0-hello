# Proj0-Hello
-------------

Author: Patrick Higgins (phiggins@cs.uoregon.edu)

Forked from https://bitbucket.org/UOCIS322/proj0-hello/

## Instructions:
---------------
- create and configure credentials.ini per the instructions in credentials-skel.ini

- no build is required; run using ``make run``

